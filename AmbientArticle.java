/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 6 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


public class AmbientArticle extends Article {

	/* Vorbedingung: name != null
	 * Nachbedingung: Erzeugt einen neuen AmbientArticle mit dem namen name.
	 */
	public AmbientArticle(String name) {
		super(name);
	}

	/* Vorbedingung: sr != null
	 * Nachbedingung: Liefert true falls alt true ist, sonst false
	 */
	@Override
	public boolean storeableIn(CooledStoreroom sr, boolean alt) {
		return alt;
	}

	/* Vorbedingung: sr != null
	 * Nachbedingung: Liefert immer true
	 */
	@Override
	public boolean storeableIn(AmbientStoreroom sr, boolean alt) {
		return true;
	}

	/* Vorbedingung: sr != null
	 * Nachbedingung: Liefert immer false
	 */
	@Override
	public boolean storeableIn(Frozen18Storeroom sr, boolean alt) {
		return false;
	}

	/* Vorbedingung: sr != null
	 * Nachbedingung: Liefert immer false
	 */
	@Override
	public boolean storeableIn(Frozen30Storeroom sr, boolean alt) {
		return false;
	}

}
