/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 6 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


public abstract class Article {

	/* Invariante: serial >= 0 */
	private static int serial = 0;
	
	/* Invariante: id > 0 */
	private int id;
	private String name;
	
	/* Vorbedingung: name != null
	 * SSHC: serial wird bei jedem Aufruf incrementiert.
	 * Nachbedingung: Erzeugt einen neuen Article mit dem namen name, und
	 * setzt die setzt die Seriennumer des Artikels.
	 */
	public Article(String name) {
		this.id = ++serial;
		this.name = name;
	}
	
	/* Nachbedingung: Liefert die Seriennummer des Artikels. Die gelieferte
	 * Zahl ist immer größer als 0
	 */
	public int getSerial() {
		return id;
	}
	
	/* Nachbedingung: Liefert eine darstellung als Zeichenkette des Artikels,
	 * in folgender Form:
	 * 	serial: x, name: y
	 * Wobei x die Seriennummer ist, und y der ganze Name des Artikels.
	 */
	@Override
	public String toString() {
		return "serial: " + id + ", name: " + name;
	}
	
	/* Vorbedingung: sr != null
	 * Nachbedingung: Liefert true wenn dieser Artikel normal in sr gelagert
	 * werden kann, oder falls alt true ist und sr ein alternativer 
	 * Lagerplatz für diesen Artikel ist. 
	 */
	public abstract boolean storeableIn(CooledStoreroom sr, boolean alt);
	public abstract boolean storeableIn(AmbientStoreroom sr, boolean alt);
	public abstract boolean storeableIn(Frozen18Storeroom sr, boolean alt);
	public abstract boolean storeableIn(Frozen30Storeroom sr, boolean alt);
	
}
