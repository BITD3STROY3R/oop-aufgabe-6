/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 6 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


public abstract class Storeroom {

	private Article a = null;
	
	/* Nachbedingung: Liefert true falls dieser Lagerplatz leer ist */
	public boolean isEmpty() {
		return a == null;
	}
	
	/* Nachbedingung: Entfernt den eingelagerten Artikel und liefert eine
	 * Referenz darauf. Falls kein Artikel gelagert war wird null geliefert.
	 */
	public Article remove() {
		Article a = this.a;
		this.a = null;
		return a;
	}
	
	/* Nachbedingung: Liefert eine Referenz auf den gelagerten Artikel, oder
	 * null falls kein Artikel in diesem Lagerplatz gelagert ist.
	 */
	public Article getArticle() {
		return a;
	}
	
	/* Vorbedingung: a != null
	 * Lagert a ein, falls dieser Lagerplatz leer ist und der Artikel a in
	 * diesem Lagerplatz regulär (alt == false) oder alternativ gelagert
	 * werden kann. Liefert true wenn a eingelagert werden konnte.
	 */
	public boolean store(Article a, boolean alt) {
		boolean stored = false;
		if (isEmpty() && canStore(a, alt)) {
			stored = true;
			this. a = a;
		}
		
		return stored;
	}
	
	/* Vorbedingung: a != null
	 * Nachbedingung: Liefert true falls a in diesem Lagerplatz regulär
	 * (alt == false) oder alternativ gelagert werden kann.
	 */
	public abstract boolean canStore(Article a, boolean alt);
	
	/* Vorbedingung: u != null
	 * Nachbedingung: Meldet u die aktuelle Benutzung dieses Lagerplatzes
	 */
	public abstract void reportUtilization(Warehouse.Utilization u);
	
}
