/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 6 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.ArrayList;
import java.util.List;

public class Test { 
	
	private static final int pageWidth   = 45;
	private static final char headerChar = '=';
	private static final char sectionChar = '-';
	private static final int headerWidth = 45;
	private static final int labelMargin = 4;
	
	/* Vorbedingung: n >= 0
	 * Nachbedingung: Liefert einen String bestehend aus der N-Fachen
	 * wiederholung des Zeichens c.
	 */
	private static String repeat(int n, char c) {
		String result = "";

		for (int i = 0; i < n; i++) {
			result += c;
		}

		return result;
	}

	/* Vorbedingung: label != null
	 * Nachbedingung: Liefert einen String der maximal headerWidth Zeichen
	 * lang ist. Der String beginnt und endet mit maximal labelMargin vielen
	 * Leerzeichen. Die Anzahl der nachfolgenden Leerzeichen ist immer gleich
	 * der der Fuehrenden.
	 */
	private static String makeLabel(String label) {
		if (label.length() <= headerWidth) {
			int i = 0;
			while (i < labelMargin && label.length() <= headerWidth - 2) {
				label = " " + label + " ";
				i++;
			}
		} else {
			label = label.substring(0, headerWidth);
		}

		return label;
	}

	/* Vorbedingung: label != null
	 * Nachbedingung: Liefert einen String welcher eine formatierte
	 * Ueberschrift darstellt. Label ist der Text der Ueberschrift.
	 */
	private static String makeHeader(String label) {
		String header = "";

		header += repeat(headerWidth, headerChar) + '\n';
		label = makeLabel(label);
		header += repeat((headerWidth - label.length()) / 2, headerChar);
		header += label;
		header += repeat((headerWidth - label.length()) / 2
				+ (headerWidth - label.length()) % 2, headerChar);
		header += '\n';
		header += repeat(headerWidth, headerChar);
		header += "\n";
		
		return header;
	}

	/* Nachbedingung: Liefert einen String welcher eine Abtrennungszeile
	 * darstellt. Diese Zeile hat eine Breite von genau pageWidth zeichen.
	 */
	private static String makeSectionbar() {
		return repeat(pageWidth, sectionChar);
	}
	
	public static void main(String[] args){
		test1();
		test2();
		test3();
	}
	
	
	private static List<Article> generateArticleList(int ambient, int cooled, int frozen18, int frozen30){
		ArrayList<Article> articles = new ArrayList<Article>();
		
		for (int i = 0; i < ambient; i++) {
			articles.add(new AmbientArticle("Ambient Article " + (i+1)));
		}
		
		for (int i = 0; i < cooled; i++) {
			articles.add(new CooledArticle("Cooled Article " + (i+1)));
		}
		
		for (int i = 0; i < frozen18; i++) {
			articles.add(new Frozen18Article("Frozen18 Article " + (i+1)));
		}
		
		for (int i = 0; i < frozen30; i++) {
			articles.add(new Frozen30Article("Frozen30 Article " + (i+1)));
		}		
		
		return articles;
	}
	
	private static void bulkStoreArticles(Warehouse wh, List<Article> articles){
		for(Article a : articles){
			if(wh.store(a)){
				System.out.println("Stored Article " + a);
			}
			else{
				System.out.println("Could not store Article " + a);
			}
		}
	}
	
	
	private static void test1(){
		System.out.println(makeHeader("Test1"));
		
		System.out.println("creating new Warehouse with 5 slots each.");
		Warehouse wh = new Warehouse(5, 5, 5, 5);
		
		System.out.println("Storing 5 ambient articles and 3 articles of the other types.");
		System.out.println(makeSectionbar());
		
		List<Article> articles = generateArticleList(5, 3, 3, 3);
		bulkStoreArticles(wh, articles);
		wh.utilisation();
		
		System.out.println(makeSectionbar());	
		
		System.out.println("Removing 1 ambient Article.");
		wh.remove(articles.get(1).getSerial());
		
		System.out.println(makeSectionbar());
		wh.utilisation();
		System.out.println(makeSectionbar());	
		
		System.out.println("Storing 2 ambient Articles.");
		System.out.println(makeSectionbar());
		
		articles = generateArticleList(2, 0, 0, 0);	
		bulkStoreArticles(wh, articles);
		
		wh.utilisation();
		System.out.println(makeSectionbar());
		
		System.out.println("Storing 2 ambient Articles. (1 should fail)");
		System.out.println(makeSectionbar());
		
		articles = generateArticleList(2, 0, 0, 0);	
		bulkStoreArticles(wh, articles);

		wh.utilisation();
		System.out.println(makeSectionbar());
		
		System.out.println("Storing 4 frozen18 Articles. 1 Frozen30 (should fail)");
		System.out.println(makeSectionbar());
		
		articles = generateArticleList(0, 0, 4, 1);	
		bulkStoreArticles(wh, articles);

		wh.utilisation();
		System.out.println(makeSectionbar());
		
	}
	private static void test2(){
		System.out.println(makeHeader("Test2"));
		
		System.out.println("creating new Warehouse with 1 slot each.");
		Warehouse wh = new Warehouse(1, 1, 1, 1);
		
		System.out.println("Storing 2 ambient articles and 2 Frozen18 articles. (1 should fail)");
		System.out.println(makeSectionbar());
		
		List<Article> articles = generateArticleList(2, 0, 2, 0);
		bulkStoreArticles(wh, articles);
		wh.utilisation();
		
		System.out.println(makeSectionbar());	
		
		System.out.println("Removing 1 ambient/frozen18 Article.");
		wh.remove(articles.get(1).getSerial());
		wh.remove(articles.get(3).getSerial());
		
		System.out.println(makeSectionbar());
		wh.utilisation();
		System.out.println(makeSectionbar());	
		
		System.out.println("Storing 2 cooled Articles. (1 should fail)");
		System.out.println(makeSectionbar());
		
		articles = generateArticleList(0, 2, 0, 0);	
		bulkStoreArticles(wh, articles);
		
		wh.utilisation();
		System.out.println(makeSectionbar());
		
		System.out.println("Storing 2 frozen18 Articles. (1 should fail)");
		System.out.println(makeSectionbar());
		
		articles = generateArticleList(0, 0, 2, 0);	
		bulkStoreArticles(wh, articles);

		wh.utilisation();
		System.out.println(makeSectionbar());		
	}
	private static void test3(){
		System.out.println(makeHeader("Test3"));
		
		System.out.println("creating new Warehouse with 3 slots each.");
		Warehouse wh = new Warehouse(3, 3, 3, 3);
		
		System.out.println("Storing 3 cooled articles and 3 frozen30 articles.");
		System.out.println(makeSectionbar());
		
		List<Article> articles = generateArticleList(0, 3, 0, 3);
		bulkStoreArticles(wh, articles);
		wh.utilisation();
	
		System.out.println(makeSectionbar());	
	
		
		System.out.println("Storing 4 ambient/frozen18 Articles. (1 of each should fail)");
		System.out.println(makeSectionbar());
		
		articles = generateArticleList(4, 0, 4, 0);	
		bulkStoreArticles(wh, articles);
		
		wh.utilisation();
		System.out.println(makeSectionbar());
		
	
		
	}
}