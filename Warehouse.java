/*
 * Objektorientierte Programmiertechniken ws2013 - Aufgabe 6 
 * =========================================================
 *
 *
 * Copyright 2013 Jannik Vierling e1226434@student.tuwien.ac.at,
 *		  Bernd Bogomolov e1225256@student.tuwien.ac.at,
 *		  Stefan Hanreich e1227486@student.tuwien.ac.at
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import java.util.Collection;
import java.util.LinkedList;
import java.util.Iterator;

public class Warehouse {
	
	private Collection<Storeroom> storerooms;
	
	private int ambient  = 0;
	private int cooled   = 0;
	private int frozen18 = 0;
	private int frozen30 = 0;
	
	public class Utilization {
		private int ambient  = 0;
		private int cooled   = 0;
		private int frozen18 = 0;
		private int frozen30 = 0;
		
		private Utilization() { }
		
		/* Nachbedingung: Erhöht den Zähler der belegten Lagerplätze bei
		 * Raumtemperatur um eins.
		 */
		public void incAmbientUtilization() {
			ambient++;
		}
		
		/* Nachbedingung: Erhöht den Zähler der belegten gekühlten Lagerplätze
		 * um eins.
		 */
		public void incCooledUtilization() {
			cooled++;
		}
		
		/* Nachbedingung: Erhöht den Zähler der belegten Lagerplätze bei
		 * minus 18 Grad um eins.
		 */
		public void incFrozen18Utilization() {
			frozen18++;
		}
		
		/* Nachbedingung: Erhöht den Zähler der belegten Lagerplätze bei
		 * minus 30 Grad um eins.
		 */
		public void incFrozen30Utilization() {
			frozen30++;
		}
		
		/* Nachbedingung: Liefert die Anzahl der belegten Lagerplätze
		 * mit Raumtemperatur.
		 */
		public int getAmbient() {
			return ambient;
		}
		
		/* Nachbedingung: Liefert die Anzahl der belegten gekühlten
		 * Lagerplätze.
		 */
		public int getCooled() {
			return cooled;
		}
		
		/* Nachbedingung: Liefert die Anzahl der belegten Lagerplätze mit
		 * minus 18 Grad.
		 */
		public int getFrozen18() {
			return frozen18;
		}
		
		/* Nachbedingung: Liefert die Anzahl der belegten Lagerplätze mit
		 * minus 18 Grad.
		 */
		public int getFrozen30() {
			return frozen30;
		}
	}
	
	/* Vorbedingung: ambient >= 0, coold >= 0, frozen18 >= 0, frozen30 >= 0
	 * Nachbedingung: Erzeugt ein neues Warehouse welches genau ambient
	 * Lagerplätze bei Raumtemperatur, cooled gekühlte Lagerplätze,
	 * frozen18 Lagerplätze bei minus 18 Grad und frozen30 Lagerplätze bei
	 * minus 30 Grad besitzt. Alle diese Lagerplätze sind frei.
	 */
	public Warehouse(int ambient, int cooled, int frozen18, int frozen30) {
		
		storerooms = new LinkedList<Storeroom>();
		
		for (int i = 0; i < ambient; i++) {
			storerooms.add(new AmbientStoreroom());
		}
		for (int i = 0; i < cooled; i++) {
			storerooms.add(new CooledStoreroom());
		}
		for (int i = 0; i < frozen18; i++) {
			storerooms.add(new Frozen18Storeroom());
		}
		for (int i = 0; i < frozen30; i++) {
			storerooms.add(new Frozen30Storeroom());
		}
		
		this.ambient  = ambient;
		this.cooled   = cooled;
		this.frozen18 = frozen18;
		this.frozen30 = frozen30;
	}

	/* Nachbedingung: Gibt am Bildschirm, die Auslastung der verschiedenen
	 * Arten von Lagerplätzen in folgender Form aus
	 * ambient: r/s, cooled t/u, frozen18 v/w, frozen30 x/y
	 * Wobei r,t,v,x die momentanen Belegungen, und s,u,w,y die maximalen
	 * Plätze pro Sort Lagerplatz sind.
	 */
	public void utilisation() {
		Utilization u = new Utilization();
		
		for (Storeroom sr : storerooms) {
			sr.reportUtilization(u);
		}
		
		System.out.println("ambient: " + u.getAmbient() + "/" + ambient
				+ ", cooled: " + u.getCooled() + "/" + cooled
				+ ", frozen18: " + u.getFrozen18() + "/" + frozen18
				+ ", frozen30: " + u.getFrozen30() + "/" + frozen30);
	}
	
	/* Vorbedingung: Article != null
	 * Nachbedingung: Versucht den Artikeln a in einem passenden Lagerplatz
	 * zu speichern. Falls kein passender Lagerplatz frei ist wird versucht
	 * Artikel die bei Raumtemperatur gelagert werden in einen gekühlten
	 * Lagerraum, und Artikel die bei minus 18 Grad gelagert werde in einen
	 * Lagerraum mit minus 30 Grad zu legen.
	 * Wurde der Artikel a eingelagert wird true geliefert.
	 */
	public boolean store(Article a) {
		boolean stored = false;
		Iterator<Storeroom> it = null;
		
		/* Try to store article in regular Storeroom */
		it = storerooms.iterator();
		while (!stored && it.hasNext()) {
			stored = it.next().store(a, false);
		}
		/* Try to store in an alternative Storeroom */
		it = storerooms.iterator();
		while (!stored && it.hasNext()) {
			stored = it.next().store(a, true);
		}
		
		return stored;
	}
	
	/* Nachbedingung: Entfernt den Artikel mit der Seriennummer serial
	 * und liefert eine Referenz auf diesen Artikel. Falls es keinen
	 * Artikel mit der Seriennummer serial gibt so wird null zurückgegeben. 
	 */
	public Article remove(int serial) {
		Article removed = null;
		boolean found   = false;
		
		Iterator<Storeroom> it = storerooms.iterator();
		while (!found && it.hasNext()) {
			Storeroom sr = it.next();
			if (!sr.isEmpty() && sr.getArticle().getSerial() == serial) {
				removed = sr.remove();
				found = true;
			}
		}
		return removed;
	}
	
	/* Nachbedingung: Gibt am Bildschirm eine Liste aller in diese Lager
	 * gespeicherter Artikel in folgender Form aus.
	 * serial: xxxx, name = "xxxx"
	 * serial: yyyy, name = "yyyy"
	 * ...
	 * Falls das Lager leer ist, wird nichts ausgegeben.
	 */
	public void inventar() {
		for (Storeroom sr : storerooms) {
			if (!sr.isEmpty()) {
				System.out.println(sr.getArticle());
			}
		}
	}
	
}
